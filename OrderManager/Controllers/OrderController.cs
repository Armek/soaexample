﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel;
using ModelLibrary;
using RestSharp;

namespace OrderManager.Controllers
{
    /// <summary>
    /// Simple order controller- no layers since this is just an example
    /// </summary>
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        
        private readonly DatabaseContext _db;
        
        public OrderController(DatabaseContext context)
        {
            _db = context;
        }

        /// <summary>
        /// get order details by id
        /// </summary>
        /// <param name="id">The order id</param>
        [HttpGet("{id}")]
        public Order GetOrder([FromRoute]int id)
        {
            return _db.Orders.Find(id);
        }

        /// <summary>
        /// takes an order
        /// </summary>
        /// <param name="order">the order to save</param>
        [HttpPost]
        public Order PostOrder([FromBody] Order order)
        {
            var restClient = new RestClient("http://localhost:5001/api");
            
            var request = new RestRequest("Customer/{id}", Method.GET);
            request.AddParameter("id", order.CustomerId, ParameterType.UrlSegment);

            IRestResponse<Customer> response = restClient.Execute<Customer>(request);

            if (response.Data == null)
                throw new Exception("Bad customer id");

            _db.Orders.Add(order);
            _db.SaveChanges();
            
            return order;
        }
    }
}