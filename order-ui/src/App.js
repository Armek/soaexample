import React, {Component} from 'react';
import './App.css';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            submittedOrders: [],
            customerId: null,
            item: null,
            quantity: null
        }

        this.handleId = this.handleId.bind(this);
        this.handleItem = this.handleItem.bind(this);
        this.handleQuantity = this.handleQuantity.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleId(event){
        this.setState({
            customerId: event.target.value
        });
    }

    handleItem(event){
        this.setState({
            item: event.target.value
        });
    }

    handleQuantity(event){
        this.setState({
            quantity: event.target.value
        });
    }

    async handleSubmit(event){

        let response = await fetch('http://localhost:5002/api/Order', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                customerId: this.state.customerId,
                quantity: this.state.quantity,
                product: this.state.item
            })
        });

        let json = await response.json();

        this.setState({
            submittedOrders: this.state.submittedOrders.concat([JSON.stringify(json)])
        });
    }

    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <h2>Enter an Order</h2>
                </div>
                <input value={this.state.customerId} onChange={this.handleId} placeholder="Your Customer Id"/>
                <input value={this.state.item} onChange={this.handleItem} placeholder="The item you want"/>
                <input value={this.state.quantity} onChange={this.handleQuantity} placeholder="The quantity you want"/>
                <button onClick={this.handleSubmit}>Submit</button>
                <div>
                    <p>Your server responses:</p>
                    {this.state.submittedOrders.map(x => <p>{x.toString()}</p>)}
                </div>
            </div>
        );
    }
}

export default App;
