﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using ModelLibrary;

namespace CustomerManager.Controllers
{
    /// <summary>
    /// Simple customer controller- no layers since this is just an example
    /// </summary>
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {

        private readonly DatabaseContext _db;
        
        public CustomerController(DatabaseContext context)
        {
            _db = context;
        }
        
        /// <summary>
        /// Gets a customer by id
        /// </summary>
        /// <param name="id">The id of the customer to get</param>
        /// <remarks></remarks>
        [HttpGet("{id}")]
        public Customer Get([FromRoute]int id)
        {
            return _db.Customers.Find(id);
        }

        /// <summary>
        /// Add a new customer
        /// </summary>
        /// <param name="customer">The customer to add</param>
        [HttpPost]
        public Customer Post([FromBody] Customer customer)
        {
            _db.Customers.Add(customer);
            _db.SaveChanges();
            
            return customer;
        }
    }
}