﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Security;

namespace ModelLibrary
{
    public class Order
    {
        [Key]
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int Quantity { get; set; }
        public string Product { get; set; }
    }
}